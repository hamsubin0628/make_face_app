import 'package:flutter/material.dart';

class ComponentAppbarNormal extends StatefulWidget {
  const ComponentAppbarNormal({
    super.key,
    required this.title
  });

  final String title;

  @override
  State<ComponentAppbarNormal> createState() => _ComponentAppbarNormalState();
}

class _ComponentAppbarNormalState extends State<ComponentAppbarNormal> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      backgroundColor: Colors.lightBlueAccent,
      foregroundColor: Colors.white,
      title: Text(
          widget.title,
        style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.w500,
          color: Colors.white,
        ),),
    );
  }
}
