import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:make_face_app/pages/page_make_face.dart';
import 'dart:math';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  final _formKey = GlobalKey<FormBuilderState>();
  TextEditingController tec = TextEditingController();

  int clothesNumber = 1;
  int eyebrowNumber = 1;
  int eyesNumber = 1;
  int hairNumber = 1;
  int mouseNumber = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.lightBlueAccent,
        foregroundColor: Colors.white,
        title: const Text(
          '친구 이름을 입력해 주세요',
          style: TextStyle(
            fontSize: 20,
            letterSpacing: -1.0,
            fontWeight: FontWeight.w500,
            color: Colors.white,
          ),
        ),
      ),

      body: SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
                    child: Column(
                      children: [
                        Text(
                            '☆★친구★☆',
                        style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.w600,
                          letterSpacing: -3.5,
                          color: Colors.pinkAccent
                        ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                    child: Column(
                      children: [
                        Text(
                          '얼굴 만들기',
                          style: TextStyle(
                              fontSize: 60,
                              fontWeight: FontWeight.w600,
                              letterSpacing: -3.5,
                              color: Colors.lightBlueAccent
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
              child: Column(
                children: [
                  FormBuilder(
                    key: _formKey,
                    child: Column(
                      children: [
                        FormBuilderTextField(
                          name: 'friendName',
                          decoration: const InputDecoration(labelText:
                          '  이름',
                          labelStyle: TextStyle(
                            fontSize: 18,
                            color: Colors.black26,
                          ),
                          ),
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(),
                          ]),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                          child: Column(
                            children: [
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.lightBlueAccent,
                                  elevation: 10.0,
                                  minimumSize: const Size(400, 60),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5)
                                  ),
                                ),
                                onPressed: () {
                                  //만약에.. 폼에 값이 있으면.. (값이 없으면 false가 리턴되어서 if문이 실행되지 않음)
                                  if (_formKey.currentState?.saveAndValidate() ?? false) {
                                    print(_formKey.currentState!.fields['friendName']!.value);
                                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageMakeFace(friendName: tec.text)));
                                  }
                                },
                                child: const Text(
                                  '시작하기',
                                  style: TextStyle(
                                      letterSpacing: -1.0,
                                      fontSize: 22,
                                      color: Colors.white
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
