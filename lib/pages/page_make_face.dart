import 'package:flutter/material.dart';
import 'package:make_face_app/pages/page_index.dart';
import 'dart:math';

class PageMakeFace extends StatefulWidget {
  const PageMakeFace({
    super.key,
    required this.friendName,
  });

  final String friendName;


  @override
  State<PageMakeFace> createState() => _PageMakeFaceState();
}

class _PageMakeFaceState extends State<PageMakeFace> {
  int mouseNumber = Random().nextInt(4) + 1;
  int clothesNumber = Random().nextInt(5) + 1;
  int eyebrowNumber = Random().nextInt(5) + 1;
  int eyesNumber = Random().nextInt(5) + 1;
  int hairNumber = Random().nextInt(5) + 1;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.lightBlueAccent,
        foregroundColor: Colors.white,
        title: const Text(
          '★친구 얼굴 완성★',
          style: TextStyle(
            fontSize: 20,
            letterSpacing: -1.0,
            fontWeight: FontWeight.w500,
            color: Colors.white,
          ),
        ),
      ),
    body: SingleChildScrollView(
      child: Column(
        children: [
          Center(
            child: Column(
              children: [
                Stack(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 100, 0, 0),
                      child: Column(
                        children: [
                          Image.asset(
                            'assets/body.png',
                            width: 300,
                            height: 300,
                            fit: BoxFit.cover,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 100, 0, 0),
                      child: Column(
                        children: [
                          Image.asset(
                            'assets/eyes/eyes$eyesNumber.png',
                            width: 300,
                            height: 300,
                            fit: BoxFit.cover,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 100, 0, 0),
                      child: Column(
                        children: [
                          Image.asset(
                            'assets/hair/hair$hairNumber.png',
                            width: 300,
                            height: 300,
                            fit: BoxFit.cover,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 100, 0, 0),
                      child: Column(
                        children: [
                          Image.asset(
                            'assets/mouse/mouse$mouseNumber.png',
                            width: 300,
                            height: 300,
                            fit: BoxFit.cover,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 100, 0, 0),
                      child: Column(
                        children: [
                          Image.asset(
                            'assets/eyebrow/eyebrow$eyebrowNumber.png',
                            width: 300,
                            height: 300,
                            fit: BoxFit.cover,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 100, 0, 0),
                      child: Column(
                        children: [
                          Image.asset(
                            'assets/clothes/clothes$clothesNumber.png',
                            width: 300,
                            height: 300,
                            fit: BoxFit.cover,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: Column(
                    children: [
                      Text(
                        widget.friendName,
                        style: TextStyle(
                          fontSize: 30,
                          letterSpacing: -1.5,
                          fontWeight: FontWeight.w500,
                          color: Colors.lightBlueAccent,
                        ),
                      ),
                    ],
                  ),
                ),
                Center(
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.lightBlueAccent,
                              onPrimary: Colors.white,
                              elevation: 10.0,
                              minimumSize: const Size(356, 60),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5),
                              )
                          ),
                          onPressed: () {
                            Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => PageIndex()
                            ),
                            );
                          },
                          child: const Text(
                            '다시하기',
                            style: TextStyle(
                                letterSpacing: -1.0,
                                fontSize: 22,
                                color: Colors.white
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    ),
    );
  }
}